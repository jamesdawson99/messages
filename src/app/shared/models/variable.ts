export class Variable {
    public name: string;
    public tooltip: string;
    public var_string: string;
    public preview: string;

    constructor(name: string, tooltip: string, var_string: string, preview: string) {
      this.name = name;
      this.tooltip = tooltip;
      this.var_string = var_string;
      this.preview = preview;
    }
}
