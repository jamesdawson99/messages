import { Variable } from './../models/variable';
import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
 name: 'searchFilter'
})

@Injectable()
export class SearchFilterPipe implements PipeTransform {
variables: Variable[];
 transform(items: any[], field: string, value: string): any[] {
    if (!items) return [];
    
    items.filter((item) => {
        return item.name === value;
   });
   console.log(items);
 }
}
