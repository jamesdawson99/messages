import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  @Output() handleSubmit = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    this.handleSubmit.emit(form.value);
  }
}
