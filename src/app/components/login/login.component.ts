import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from './../../shared/models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public message: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  handleSubmit(event) {
    const user: User = event;
    if (user.username === 'james' || user.password === 'james') {
      this.router.navigate(['dashboard']);
    } else {
      this.message = 'Username or password is incorrect';
    }
  }
}
