import { Variable } from './../../shared/models/variable';
import { VariableService } from './variables.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sms',
  templateUrl: './sms.component.html',
  styleUrls: ['./sms.component.scss']
})
export class SmsComponent implements OnInit {

  vars: Variable[];
  template: string;
  chars: number;
  smsCount: number;
  constructor(private variableService: VariableService) { }

  ngOnInit() {
  }
  setTemplate(template: string) {
      this.template = this.previewTemplate(template);
      this.chars = this.countTemplate(this.template);
      this.smsCount = this.smsCounter(this.chars);
  }
  previewTemplate(temp: string){
      this.vars = this.variableService.getVariables(); // gets Variables
      for (let i = 0; i < this.vars.length; i++ ) {
          while (temp.includes(this.vars[i].var_string)) { // while template includes variable continue replacing
              const re = new RegExp(this.escapeRegExp(this.vars[i].var_string)); // creates regex expression out of variable_string
              temp = temp.replace(re, this.vars[i].preview); // replaces match of var_string with preview
          }
      }
      return temp;
  }
  escapeRegExp(str: string) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
  }
  countTemplate(temp: string) {
    return temp.length;
  }
  smsCounter(chars: number){
    let sms_count = Math.round(chars / 160);
    if (sms_count === 0) { sms_count = 1; }
    return sms_count;
  }
}
