import { VariableService } from './../variables.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sms-preview',
  templateUrl: './sms-preview.component.html',
  styleUrls: ['./sms-preview.component.scss']
})
export class SmsPreviewComponent implements OnInit {
  template: string;
  @Input() newTemplate: string;

  constructor(private variableService: VariableService) { }

  ngOnInit() {
    this.newTemplate = 'SMS template preview.';
    this.template = this.newTemplate;
  }

}
