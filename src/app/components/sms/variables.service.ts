import { Variable } from './../../shared/models/variable';
import { EventEmitter, OnInit } from '@angular/core';


export class VariableService {

  variables: Variable[] = [
    new Variable(
      "Add to Date",
      "adds the number of unit's (days, months, years etc) specified to the date - if date is null then use current date at send time",
      "{{ addToDate(date,number,unit) | date : 'dd/MM/yyyy'}}",
      "03/07/2017"
      ),
    new Variable(
      'Original Balance',
      'The original balance of the customers accounts',
      "{{originalBalance | currency: '£'}}",
      "£307.89"
      ),
    new Variable(
        "Original Balance Date",
        "The original date of the original balance",
        "{{originalBalanceDate | date: 'dd/MM/yyyy'}}",
        "11/12/2017"
    ),
    new Variable(
        "Client Name",
        "The client the debt was purchased from",
        "{{clientName}}",
        "Sarah Jessica Smith"
    ),
    new Variable(
        "Original Reference",
        "The original account reference",
        "{{originalReference}}",
        "3659"
    ),
    new Variable(
        "Customer Phone Number",
        "The customer's phone number",
        "{{customerPhoneNumber}}",
        "07526 279293"
    ),
    new Variable(
        "Original Creditor",
        "The original creditor the account was purchased from",
        "{{originalCreditor}}",
        "Original Creditor"
    ),
    new Variable(
        "First Name",
        "The customer's first name",
        "{{firstName}}",
        "Sarah"
    ),
    new Variable(
        "Last Name",
        "The customer's first name",
        "{{lastName}}",
        "Smith"
    ),
    new Variable(
        "Assignment Date",
        "The date assigned",
        "{{assignmentDate | date: 'dd/MM/yyyy'}}",
        "12/12/2017"
    ),
    new Variable(
        "Instruct Date",
        "The date instructed to collect",
        "{{instructDate | date: 'dd/MM/yyyy'}}",
        "03/01/2018"
    ),
    new Variable(
        "Online Pin",
        "The pin number for the customers access to online portal",
        "{{onlinePin}}",
        "9822"
    ),
    new Variable(
        "Customer Reference",
        "The customers reference number",
        "{{customerReference}}",
        "3001"
    ),
    new Variable(
        "Original Customer Reference",
        "The customers original reference number",
        "{{customerReference1}}",
        "2997"
    ),
    new Variable(
        "Customer's Email",
        "The customers email address",
        "{{customerEmail}}",
        "sarahjessicasmith@domain.com"
    ),
    new Variable(
        "Customer's Title",
        "The customers title mr/miss/mrs",
        "{{title}}",
        "Mrs"
    ),
    new Variable(
        "Current Account Balance",
        "The current balance of account",
        "{{currentBalance | currency: '£'}}",
        "£613.96"
    ),
    new Variable(
        "Accounts Total",
        "The current balance of all accounts",
        "{{totalAccounts}}",
        "3"
    ),
  ];
  customerReference1: string;
  varSelected: string;
  variableSelected = new EventEmitter<string>();
  constructor() {}

  setVariableString(var_string: string) {
      this.variableSelected.emit(var_string);
  }

  getVariables(name?: string): any {
    if (name) {
      return this.variables.filter((variable) => variable.name.toLowerCase().includes(name.toLowerCase()));
    }
    return this.variables;
  }
  getVariable(index: number) {
    return this.variables[index];
  }

}
