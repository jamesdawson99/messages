import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsDataComponent } from './sms-data.component';

describe('SmsDataComponent', () => {
  let component: SmsDataComponent;
  let fixture: ComponentFixture<SmsDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
