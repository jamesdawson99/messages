import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sms-data',
  templateUrl: './sms-data.component.html',
  styleUrls: ['./sms-data.component.scss']
})
export class SmsDataComponent implements OnInit {

  @Input() newSmsCount: number;
  @Input() newChars: number;
  constructor() { }

  ngOnInit() {
    this.newChars = 0;
    this.newSmsCount = 1;
  }

}
