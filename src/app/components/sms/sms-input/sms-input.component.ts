import { VariableService } from './../variables.service';
import { Component, OnInit, EventEmitter, Output, OnChanges } from '@angular/core';



@Component({
  selector: 'app-sms-input',
  templateUrl: './sms-input.component.html',
  styleUrls: ['./sms-input.component.scss']
})
export class SmsInputComponent implements OnInit, OnChanges{

  template: string;
  @Output() newTemplate: EventEmitter<string> = new EventEmitter();
  constructor(private variableService: VariableService) { }

  ngOnInit() {
    this.template = 'test';
    this.variableService.variableSelected
    .subscribe(
      (var_string: any) => {
        this.template = this.template + var_string;
      }
    );
  }
  ngOnChanges() {
    console.log("test");
    this.templateChanged();
  }
  templateChanged() {
    this.newTemplate.emit(this.template);
    console.log(this.template);
  }
}
