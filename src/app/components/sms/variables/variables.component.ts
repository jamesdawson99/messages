import { Variable } from './../../../shared/models/variable';
import { VariableService } from '../variables.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sms-variables',
  templateUrl: './variables.component.html',
  styleUrls: ['./variables.component.scss'],

})
export class VariablesComponent implements OnInit{
  filter: string;
  limit: number;
  variables: Variable[];
  varsLength: number;
  constructor(private variableService: VariableService) { }

  ngOnInit() {
    this.variables = this.variableService.getVariables();
    this.limit = 5;
    this.varsLength = this.variables.length;
  }
  filterVariables() {
    this.variables = this.variableService.getVariables(this.filter);
    this.limit = this.variables.length;
    console.log(this.filter);
  }
  loadMoreVars() {
      this.limit = this.limit + 5;
      if (this.limit >= this.varsLength) {
        this.limit = this.varsLength;
      }
  }
}
