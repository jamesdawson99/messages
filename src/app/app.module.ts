import { SearchFilterPipe } from './shared/pipes/search.pipe';
import { VariableService } from './components/sms/variables.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TemplatesComponent } from './components/templates/templates.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginFormComponent } from './components/login/login-form/login-form.component';
import { FormsModule } from '@angular/forms';
import { SmsComponent } from './components/sms/sms.component';
import { SmsCreateComponent } from './components/sms/sms-create/sms-create.component';
import { VariablesComponent } from './components/sms/variables/variables.component';
import { VariableLinkComponent } from './components/sms/variables/variable-link/variable-link.component';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SmsPreviewComponent } from './components/sms/sms-preview/sms-preview.component';
import { SmsInputComponent } from './components/sms/sms-input/sms-input.component';
import { SmsDataComponent } from './components/sms/sms-data/sms-data.component';

@NgModule({
  declarations: [
    AppComponent,
    TemplatesComponent,
    HeaderComponent,
    PageNotFoundComponent,
    LoginComponent,
    DashboardComponent,
    LoginFormComponent,
    SmsComponent,
    SmsCreateComponent,
    VariablesComponent,
    VariableLinkComponent,
    SearchFilterPipe,
    SmsPreviewComponent,
    SmsInputComponent,
    SmsDataComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NgbModule.forRoot()
  ],
  providers: [VariableService],
  bootstrap: [AppComponent]
})
export class AppModule { }
